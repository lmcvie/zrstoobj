#include "ZRSFileLoader.h"
#include "OBJWriter.h"
int main(int argc, char *args[])
{
	std::shared_ptr<ZRSFileLoader> zrsFileLoader;
	std::shared_ptr<OBJWriter> objWriter;


	zrsFileLoader->LoadZRS("new_church_a.zrs");
	objWriter->WriteToFile("new_church_a.obj", zrsFileLoader);


	return 0;
}