#include "ZRSFileLoader.h"
std::vector<std::vector<double>> vertices;
std::vector<std::vector<int>> indices;

std::stringstream buffer;
int pos = 0;
int endPos = 0;



ZRSFileLoader::ZRSFileLoader()
{
}


ZRSFileLoader::~ZRSFileLoader()
{
}



void ZRSFileLoader::parser()
{
	//create storage to split relevant data up

	//find size for vectors and indices for the church
	pos = buffer.str().find("size");
	pos = pos + 5; // move 5 characters down ( size + (  )

	//create substrings of the numbers and place in integer variables
	int sizeX = stoi(buffer.str().substr(pos, 3)); // size x is the number of rows for vertices, texture and normals
	int sizeX2 = stoi(buffer.str().substr(pos + 4, 3)); // size x for the tri (indices)

	// find size of the cockrel mesh verts and indices
	int tempPos = buffer.str().find("size", (pos+1));
	tempPos = tempPos + 5;
	
	int tempX = stoi(buffer.str().substr(tempPos, 2));
	int tempX2 = stoi(buffer.str().substr(tempPos + 3, 2));

	//resize vertex vector using the total size of both meshes

	vertices.resize(sizeX + tempX, std::vector<double>(8, 1)); 

	
	//resize indices vector using the total size of both meshes
	indices.resize(sizeX2 + tempX2, std::vector<int>(3, 1));

	// parse first mesh (church)
	parsingHandler(sizeX, sizeX2, 0,0);

	//change position to the second mesh (cockrel)
	pos = tempPos;
	// parse the second mesh (cockrel)
	parsingHandler((sizeX + tempX), (sizeX2 + tempX2), sizeX, sizeX2);
}


void ZRSFileLoader::parsingHandler(int indexX, int indexY, int startX,int startX2)
{

	//find pos of the word vertex in stream
	pos = buffer.str().find("vertex",pos); // position for start of value

	pos = pos + 7; // move position down to skip word vertex and bracket

	endPos = buffer.str().find(",", pos); // find end position by looking for the nearest ,

	 // parse vertex,texture and normal data
	for (int i = startX; i < indexX; ++i) // number of rows
	{

		for (int j = 0; j < 8; ++j) // number of columns
		{
			parseValue(i, j); // parse specific value

			if (j < 7) // if not the last value in the line to be parsed
			{
				pos = (pos + (endPos - pos) + 1); // move the position (+1 to avoid it finding the comma its on)
				endPos = buffer.str().find(",", pos);
			}
		}

		if (i < indexX - 1) // if not the last line of values in buffer, go to a new line then skip the word vertex again and find new end position
		{
			pos = buffer.str().find("vertex", pos); // position for start of value
			pos = pos + 7; // skip word vertex and bracket
			endPos = buffer.str().find(",", pos); // position for end of value
		}
	}

	//parse indices
	pos = buffer.str().find("tri", pos); // position for start of value
	pos = pos + 4; // move position in buffer to skip tri(



	endPos = buffer.str().find(",", pos); // find next , position and use as end position


	for (int i = startX2; i < indexY; ++i) // number of rows
	{

		for (int j = 0; j < 3; ++j) // number of columns
		{
			parseValue2(i, j); // parse specific value

			if (j < 2) // if not the last value in the line
			{
				pos = (pos + (endPos - pos) + 1); // move the position (+1 to avoid it finding the comma its on)
				endPos = buffer.str().find(",", pos); // find new end position
			}
		}

		if (i < indexY - 1) //if not the last line of values in buffer, go to next line then skip the word tri( and find new end position
		{
			pos = buffer.str().find("tri", pos); // find new start position in new line
			pos = pos + 4; // skip word skip tri(
			endPos = buffer.str().find(",", pos); // find new end position
		}

	}

}

void ZRSFileLoader::parseValue(int indexX, int indexY)
{

	std::string line = buffer.str().substr(pos, (endPos - pos)); // end pos - pos give the size of the number between the 2 commas.
	vertices[indexX][indexY] = stod(line); // place coord in first cell


}



void ZRSFileLoader::parseValue2(int indexX, int indexY)
{
	std::string line = buffer.str().substr(pos, (endPos - pos)); // end pos - pos give the size of the number between the 2 commas.
	indices[indexX][indexY] = stoi(line); // place coord in first cell


}



void ZRSFileLoader::LoadZRS(std::string name)
{
	
	// place file into a string stream
	std::ifstream is(name, std::ifstream::binary);
	
	if (is.is_open())
	{
		
		buffer << is.rdbuf(); //check if open. If it is read the data inside file into a string stream buffer
		is.close(); // close the file
	}

	//parse stringstream buffer and split data up
	parser();
	
}


std::vector<std::vector <double>> ZRSFileLoader::getVerts()
{

	return vertices; // return vertices vector
}

std::vector<std::vector<int>> ZRSFileLoader::getIndices()
{
	return indices; // return indices vector
}
