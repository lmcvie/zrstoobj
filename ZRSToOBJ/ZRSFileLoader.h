#pragma once
#include <string>



#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>

class ZRSFileLoader
{
public:
	ZRSFileLoader();
	~ZRSFileLoader();

public:
	void LoadZRS(std::string name);
	void parsingHandler(int indexX, int indexY, int startX,int startX2);
	void parser();
	void parseValue(int indexX, int indexY);
	void parseValue2(int indexX, int indexY);
	std::vector<std::vector<double>> getVerts();
	std::vector<std::vector<int>> getIndices();



private:
	
};

