#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include<sstream>
#include"ZRSFileLoader.h"
#include <memory>


class OBJWriter
{
public:
	OBJWriter();
	~OBJWriter();
	void WriteToFile(std::string name, std::shared_ptr<ZRSFileLoader> zrsfileLoader);
	std::string WriteOBJData(std::shared_ptr<ZRSFileLoader> zrsfileLoader);



public:


private:
	
};

