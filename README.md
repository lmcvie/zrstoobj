# README #


This Program was created to convert a .ZRS file (new_church_a.zrs) to OBJ format. This program uses no libraries and should run immediately and create an obj file inside the folder beside the zrs file. It can then be opened up into 3D editing programs such as blender. The program currently only works with the specific zrs file and would need some adjusting to work with all zrs files.